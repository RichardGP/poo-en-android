package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;
//Implemente la clase public interface GetCallback<DataObject>
public class DetailActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6
        //Se accede al Intent del object_id enviado
        String object_id = getIntent().getStringExtra("object_id");

        //Se crea una instacia del TextView para acceder a el layout de la descripción
        TextView description = (TextView) findViewById(R.id.description);
        //Se utiliza el metodo setMovementMethod para realizar la navegación
        //en el contenido del layout
        description.setMovementMethod(LinkMovementMethod.getInstance());

        //Se accede al producto que se quiere mostrar pantalla
        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            //Metodo para acceder a las propiedades del producto
            public void done(DataObject object, DataException e) {
                if (e== null){
                    TextView title = (TextView) findViewById(R.id.title);

                    //3.3 Se accede a los layouts creados en el activity_detail
                    TextView price = (TextView) findViewById(R.id.price);
                    ImageView thumbnail=  (ImageView) findViewById(R.id.thumbnail);
                    TextView description = (TextView) findViewById(R.id.description);

                    //3.4 Se asigna el contenido a los layouts el cual se consigue de la base de datos simulada
                    title.setText((String) object.get("name"));
                    price.setText((String) object.get("price")+"\u0024");
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));
                    description.setText((String)object.get("description"));

                }else{
                    //Error
                }
            }
        });
        // FIN - CODE6

    }



}
